package it.cnr.iit.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LogAnalyticConfig {
	@Value("${loganalytic.isi-url}")
	private String url;
	@Value("${loganalytic.isi-username}")
	private String username;
	@Value("${loganalytic.isi-password}")
	private String password;

	public String getIsiUrl() {
		return url;
	}

	public void setIsiUrl(String url) {
		this.url = url;
	}

	public String getIsiUsername() {
		return username;
	}

	public void setIsiUsername(String username) {
		this.username = username;
	}

	public String getIsiPassword() {
		return password;
	}

	public void setIsiPassword(String password) {
		this.password = password;
	}

}
