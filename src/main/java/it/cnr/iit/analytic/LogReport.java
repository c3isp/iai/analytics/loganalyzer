package it.cnr.iit.analytic;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class LogReport {
	private Set<String> filenames;
	private Set<LogEvent> events;

	public LogReport() {
		events = new HashSet<LogEvent>();
		filenames = new HashSet<String>();
	}

	public void addFailure(String type, String subject) {
		LogEvent event = getEvent(type, subject);
		event.failure();
	}

	public void addSuccess(String type, String subject) {
		LogEvent event = getEvent(type, subject);
		event.success();
	}

	public void addSeverity(String type, String subject, int severity) {
		LogEvent event = getEvent(type, subject);
		event.severity(severity);
	}

	public Set<LogEvent> getEvents() {
		return events;
	}

	public void addFileName(String filename) {
		filenames.add(filename);
	}

	public Set<String> getFilenames() {
		return filenames;
	}

	private LogEvent getEventBySubject(final String subject) {
		return events.stream().filter(o -> o.getSubject().equals(subject)).findFirst().orElse(null);
	}

	private LogEvent newEvent(String type, String subject) {
		LogEvent event = new LogEvent(type, subject);
		events.add(event);
		return event;
	}

	private LogEvent getEvent(String type, String subject) {
		LogEvent event = getEventBySubject(subject);
		if (event == null) {
			event = newEvent(type, subject);
		}
		return event;
	}

	@Override
	public String toString() {
		String data = "";
		try {
			data = new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return data;
	}
}
