package it.cnr.iit.analytic.restapi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import io.swagger.annotations.ApiModel;
import it.cnr.iit.analytic.AsyncAnalyticExecution;
import it.cnr.iit.common.analytic.AnalyticManager;
import it.cnr.iit.configurations.LogAnalyticConfig;
import it.cnr.iit.utility.UtilsForISI;

@ApiModel(value = "LogAnalitic", description = "Log Analytic for detection Bruteforce attacks")
@RestController
@RequestMapping("/v1")
public class LogAnalyticController {
	private final static Logger log = LoggerFactory.getLogger(LogAnalyticController.class);

	@Autowired
	LogAnalyticConfig config;
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	AsyncAnalyticExecution asyncAnalyticExecution;
	@Autowired
	UtilsForISI utilsForISI;

	private AnalyticManager<String> analyticManager = new AnalyticManager<String>();

	@PostMapping(value = "/detectBruteforce/{sessionId}")
	public AnalyticResult detectBruteforce(@RequestBody InputParameters inputParameters,
			@PathVariable String sessionId) {
//		ResponseEntity<String> response = null;

		Instant start = Instant.now();
		log.error("LogAnalytic parameters : " + inputParameters.toString());

		try {
			String path = inputParameters.getPath();
			if (path.isEmpty()) {
				log.error("Error VDL path is empty");
				return new AnalyticResult("error", "VDL path is empty");
			}

			URI uri = new URI(path);
			File file = Paths.get(uri.getPath()).toFile();
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));

			Future<String> future = asyncAnalyticExecution.executeWithResult(inputParameters, reader);
			analyticManager.getFutureMap().putFuture(sessionId, future);
			String result = future.get();
			log.error("result from analytic: " + result);

			byte[] fileContent = Files.readAllBytes(Paths.get(result));

			AnalyticResult analyticResult = new AnalyticResult();
			analyticResult.setStatus("ok");
			analyticResult.addToResultArray(fileContent);
			Paths.get(result).toFile().delete();
			analyticManager.getFutureMap().removeFuture(sessionId);

			String file2 = new String(analyticResult.getResultArray().get(0));
			log.error("content of the file in loganalytic: " + file2);
			Instant end = Instant.now();
			log.info("Execution time : " + Duration.between(start, end).getNano() / 1e9 + "s");
			return analyticResult;

		} catch (CancellationException ce) {
			return new AnalyticResult("error", "It was impossible to complete the analytic with sessionId " + sessionId
					+ " because it has been stopped");
		} catch (Exception e) {
			e.printStackTrace();
			return new AnalyticResult("error", e.getMessage());
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/stopAnalytic/{sessionId}")
	public String stopAnalytic(@PathVariable String sessionId) {
		log.info("Stopping analytic with sessionId=" + sessionId);
		return analyticManager.stopAnalytic(sessionId);
	}

}