package it.cnr.iit.analytic.restapi;

public class InputParameters {

	private String path;
	private int minFailures = 1;
	private double minFractionFailures = 0.5;

	public InputParameters() {
		super();
	}

	public InputParameters(String path) {
		super();
		this.path = path;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getMinFailures() {
		return minFailures;
	}

	public void setMinFailures(int minFailures) {
		if (minFailures > 0)
			this.minFailures = minFailures;
	}

	public double getMinFractionFailures() {
		return minFractionFailures;
	}

	public void setMinFractionFailures(double minFractionFailures) {
		if (minFractionFailures > 0 && minFractionFailures <= 1)
			this.minFractionFailures = minFractionFailures;
	}
	
	@Override
	public String toString() {
		return "InputParameters [" + (path != null ? "path=" + path + ", " : "") + "minFailures=" + minFailures
				+ ", minFractionFailures=" + minFractionFailures + "]";
	}
}
