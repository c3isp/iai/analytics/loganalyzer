package it.cnr.iit.analytic.restapi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

import it.cnr.iit.configurations.LogAnalyticConfig;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan("it.cnr.iit")
public class ApplicationDeployer extends SpringBootServletInitializer {
	private final static Logger LOGGER = LoggerFactory.getLogger(ApplicationDeployer.class);

	private static final String URL_PATH = "/v1/.*";

	@Value("${security.activation.status}")
	private boolean securityActivationStatus;

	@Autowired
	BuildProperties buildProperties;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ApplicationDeployer.class);
	}

	@Bean
	public Docket documentation() {
		Docket docket = new Docket(DocumentationType.SWAGGER_2);
		docket.apiInfo(metadata());
		if (!securityActivationStatus) {
			return docket.select().paths(PathSelectors.regex(URL_PATH)).build();
		} else {
			return docket
					// .securitySchemes(new ArrayList<ApiKey>(Arrays.asList(new
					// ApiKey("mykey",
					// "api_key", "header"))))
					.securitySchemes(new ArrayList<>(Arrays.asList(new BasicAuth("basicAuth"))))
					.securityContexts(new ArrayList<>(Arrays.asList(securityContext()))).select()
					.paths(PathSelectors.regex(URL_PATH)).build();
		}
	}

	private SecurityContext securityContext() {
		return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.regex(URL_PATH))
				.build();
	}

	List<SecurityReference> defaultAuth() {
		AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
		AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
		authorizationScopes[0] = authorizationScope;
		// return new ArrayList<SecurityReference>(Arrays.asList(new
		// SecurityReference("mykey", authorizationScopes)));
		return new ArrayList<>(Arrays.asList(new SecurityReference("basicAuth", authorizationScopes)));
	}

	@Bean
	public UiConfiguration uiConfig() {
		return new UiConfiguration("*");
	}

	@Autowired
	LogAnalyticConfig config;

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		return restTemplateBuilder.basicAuthentication(config.getIsiUsername(), config.getIsiPassword()).build();
	}

	private ApiInfo metadata() {
		return new ApiInfoBuilder().title("Log Analytic").description("Log Analytic for detection Bruteforce attacks")
				.version(buildProperties.getVersion())
				.contact(new Contact("Alessandro Rosetti", "", "alessandro.rosetti@iit.cnr.it")).build();
	}

	public static void main(String[] args) {
		SpringApplication.run(ApplicationDeployer.class, args);
	}

}
