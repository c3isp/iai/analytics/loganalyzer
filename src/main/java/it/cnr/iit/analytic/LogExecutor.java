package it.cnr.iit.analytic;

import java.util.concurrent.ExecutorService;

import org.springframework.stereotype.Component;

@Component
public class LogExecutor {

	private ExecutorService executor;

	public LogExecutor() {
	}

	public ExecutorService getExecutor() {
		return executor;
	}

	public void setExecutor(ExecutorService executor) {
		this.executor = executor;
	}

}
