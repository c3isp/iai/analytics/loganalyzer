package it.cnr.iit.analytic;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.jcustenborder.cef.CEFParser;
import com.github.jcustenborder.cef.CEFParserFactory;
import com.github.jcustenborder.cef.Message;

public class LogAnalytic {
	private final static Logger log = LoggerFactory.getLogger(LogAnalytic.class);

	private LogReport report;
	private CEFParser cefParser;
//	private HashMap<String, Integer> countMap = new HashMap<>();

	private int minFailuresThreshold;
	private double minFractionFailuresThreshold;

	public LogAnalytic() {
		cefParser = CEFParserFactory.create();
		report = new LogReport();
	}

	public void addFileName(String fileName) {
		report.addFileName(fileName);
	}

	public void processFile(BufferedReader bufferedReader) throws IOException {
		while (bufferedReader.ready()) {
			String line = bufferedReader.readLine();
			processCefLine(line);
		}
	}

	public boolean processCefLine(String line) {
		Message message = cefParser.parse(line);
		if (message != null) {
			Set<String> keys = message.extensions().keySet();

			if (keys.contains("src") && keys.contains("app") && keys.contains("outcome")) {
				String subject = message.extensions().get("src");
				String app = message.extensions().get("app").replace("d\"\"", "");
				if (message.extensions().get("outcome").equals("failure")) {
					report.addFailure(app, subject);
				} else {
					report.addSuccess(app, subject);
				}
				report.addSeverity(app, subject, Integer.parseInt(message.severity()));
			}
		}
		return true;
	}

//	public boolean processCefLine(String line) {
//		Message message = cefParser.parse(line);
//		if (message != null) {
//			Set<String> keys = message.extensions().keySet();
//
//			if (keys.contains("src")) {
//				String src = message.extensions().get("src");
//				if (countMap.containsKey(src)) {
//					countMap.put(src, countMap.get(src) + 1);
//				} else {
//					countMap.put(src, 1);
//				}
//			}
//		}
//		return true;
//	}

	// TODO considerare severity del messaggio. aka: se invalid user è più grave.
	public String buildCEF() {
		System.out.println(report.toString());

		StringBuilder sb = new StringBuilder();
		for (LogEvent event : report.getEvents()) {
			if (event.getFailures() >= minFailuresThreshold
					&& event.getFractionFailures() >= minFractionFailuresThreshold) {
				sb.append(event.getCEFLine() + "\n");
			}
		}
		if (sb.length() == 0) {
			sb.append("# no bruteforce attacks found");
		}

		return sb.toString();
	}

	public LogAnalytic setMinFailures(int threshold) {
		if (threshold >= 0) {
			minFailuresThreshold = threshold;
		}
		return this;
	}

	public LogAnalytic setMinFractionFailures(double threshold) {
		if (threshold >= 0 && threshold <= 1) {
			minFractionFailuresThreshold = threshold;
		}
		return this;
	}

	public String dumpCEFToFile() {
		String path = getRandomLogFilePath().toAbsolutePath().toString();
		writeToFile(path, buildCEF().getBytes());
		return path;
	}

	private Path getRandomLogFilePath() {
		long timestamp = (long) (System.currentTimeMillis() / 1e3);
		String randStr = RandomStringUtils.randomAlphanumeric(8);
		return Paths.get("/tmp/loganalytic_" + timestamp + "_" + randStr + ".log");
	}

	public static void writeToFile(String path, byte[] bytes) {
		try (FileOutputStream out = new FileOutputStream(path)) {
			out.write(bytes);
		} catch (IOException e) {
			log.error("Error writing file");
			e.printStackTrace();
		}
	}
}
