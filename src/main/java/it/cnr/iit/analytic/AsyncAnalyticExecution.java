package it.cnr.iit.analytic;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.concurrent.Future;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import it.cnr.iit.analytic.restapi.InputParameters;
import it.cnr.iit.common.analytic.AbstractAnalyticExecutor;

@Service
public class AsyncAnalyticExecution extends AbstractAnalyticExecutor {

	@Async
	@Override
	public Future<String> executeWithResult(Object... objects) {

		try {

			System.out.println("going to sleep for 30 sec...");
			Thread.sleep(30000);
			System.out.println("aweking!");
			InputParameters inputParameters = (InputParameters) objects[0];
			BufferedReader reader = (BufferedReader) objects[1];
			LogAnalytic logAnalytic = new LogAnalytic();
			logAnalytic.setMinFailures(inputParameters.getMinFailures())
					.setMinFractionFailures(inputParameters.getMinFractionFailures());
			logAnalytic.processFile(reader);

			String outputFile = logAnalytic.dumpCEFToFile();

			return new AsyncResult<String>(outputFile);
		} catch (IOException e) {
			e.printStackTrace();
			return new AsyncResult<String>("ERROR: " + e.getMessage());
		} catch (InterruptedException e) {
			e.printStackTrace();
			return new AsyncResult<String>("ERROR: " + e.getMessage());
		}

	}

	@Override
	public void execute() {
	}

	@Override
	public <T> T executeWithResult() {
		return null;
	}

	@Override
	public void execute(Object... objects) {

	}

}
