package it.cnr.iit.analytic;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class LogEvent {
	private static final String CEF_PREFIX = "CEF:0|Security|LogAnalyzerReport|1.0|100|ssh report item|10|";

	private String subject = "";
	private String type = "";

	private int failures = 0;
	private int successes = 0;
	private int severityCount = 0;

	public LogEvent() {

	}

	public LogEvent(String type, String subject) {
		this.type = type;
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getFailures() {
		return failures;
	}

	public void failure() {
		failures += 1;
	}

	public int getSuccesses() {
		return successes;
	}

	public void success() {
		successes += 1;
	}

	public void severity(int severity) {
		severityCount += severity;
	}

	public float getSeverityAverage() {
		int tot = successes + failures;
		tot = tot > 0 ? tot : 1;
		return severityCount / (float)tot;
	}

	public float getFractionFailures() {
		float val = (float) 1.0;

		if (failures > 0) {
			val = (float) failures / (float) (failures + successes);
		} else if (successes > 0) {
			val = 0;
		}

		return val;
	}

	@JsonIgnore
	public final String getCEFLine() {
		return CEF_PREFIX + "src=" + getSubject() + " app=" + getType();
	}
}
