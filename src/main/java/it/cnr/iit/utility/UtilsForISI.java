package it.cnr.iit.utility;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import it.cnr.iit.configurations.LogAnalyticConfig;

@Component
public class UtilsForISI {

	@Autowired
	RestTemplate restTemplate;
	@Autowired
	LogAnalyticConfig config;

	private final static Logger LOGGER = LoggerFactory.getLogger(UtilsForISI.class);

	public ResponseEntity<String> createDPO(String logFilePath, String sessionId) {
		String message;
		try {
			MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
			parts.add("input_metadata",
					"{ \"Request\": { \"Attribute\": [ { \"AttributeId\": \"ns:c3isp:dpo-metadata\",\"Value\":"
							+ " \"{\\\"id\\\":\\\"9924000123\\\",\\\"dsa_id\\\":\\\"DSA-c7aa89e7-ed98-42a8-91cf-74876c7ebc3e\\\",\\\"start_time\\\":"
							+ "\\\"2017-12-14T12:00:00.0Z\\\",\\\"end_time\\\":\\\"2017-12-14T18:01:01.0Z\\\",\\\"event_type\\\":\\\"netflow\\\","
							+ "\\\"organization\\\":\\\"ISP@CNR\\\"}\", \"DataType\": \"string\"      }, "
							+ " { \"AttributeId\": \"urn:oasis:names:tc:xacml:3.0:subject:access-purpose\",      "
							+ "  \"Value\": \"Cyber Threat Monitoring\",  \"DataType\": \"string\"      }, "
							+ " {\"AttributeId\":\"urn:oasis:names:tc:xacml:1.0:action:iai:session-id\",\"Value\":\""
							+ sessionId + "\",\"DataType\":\"string\"}  ]  }}");
			parts.add("fileToSubmit", new FileSystemResource(new File(logFilePath)));

			String url = config.getIsiUrl() + "dpo";
			ResponseEntity<String> response = restTemplate.postForEntity(url, parts, String.class);

			message = response.getBody();
			if (response.getStatusCode() == HttpStatus.OK) {
				LOGGER.info("DPO created correctly : " + message);
				return new ResponseEntity<>(message, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			message = "isi-api error : " + e.getMessage();
		}

		LOGGER.error("[ERROR] Contacting the remote API... : " + message);
		return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
	}
}
